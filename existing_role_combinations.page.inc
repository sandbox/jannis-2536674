<?php


/*
* Custom page callback for webform_submission_cleaner
*/

function existing_role_combinations_page() {

  return drupal_get_form('form_existing_role_combinations');
 }

function form_existing_role_combinations($form_state) {
  $usercount = db_query('SELECT uid FROM {users}')->rowCount();
  $rolecount = db_query('SELECT rid FROM {role}')->rowCount();
  $rcidcount = db_query('SELECT rcid FROM {existing_role_combinations}')->rowCount();

  $form = array();

  $form['current_stats'] = array(
    '#type' => 'item',
    '#title' => t('Existing Role Combinations stats'),
    '#markup' => 'Total users:  ' . $usercount . '<br>Total roles:  ' . $rolecount . '<br>Cartesian Product:  ' . pow(2, $rolecount) . '<br>Existing Unique Role Combinations in database : ' . $rcidcount,
  );
  $form ['instructions'] = array(
    '#type' => 'item',
    '#title' => t('Instructions'),
    '#markup' => t('Enter a range of uids to check.  Leave it blank to go over the whole user table.  Be warned, it may take a while.'),
  );

  $form['starting_uid'] = array(
    '#type' => 'textfield',
    '#title' => 'Starting uid  to check',
    '#default_value' => NULL,
    '#size' => 10,
  );
    $form['ending_uid'] = array(
    '#type' => 'textfield',
    '#title' => 'Ending uid  to check',
    '#default_value' => NULL,
    '#size' => 10,
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
 }




 //Validate the form state from the user
 function form_existing_role_combinations_validate($form, $form_state) {
    $query = db_select('users');
    $query->addExpression('MAX(uid)');
    $max = $query->execute()->fetchField();

    $start = $form_state['values']['starting_uid'];
  $end = $form_state['values']['ending_uid'];

  batch_set(batch_identify_unique_role_combinations($start, $end, $max));
}


function indentify_unique_role_combinations($uid, &$context) {
  $context['message'] = 'checking if uid is legal';
  $user = user_load($uid);
  if (!is_object($user)) {
    $context['message'] = 'uid is not legal, skipping';
  }
  else {
    $context['message'] = 'uid is legal checking if exists in table';
    $this_user_rids = array_flip($user->roles);

    //remove 'authenticated user role' from this list it's always first
    $this_user_rids = array_slice($this_user_rids, 1);
    $this_user_rids = implode(', ', $this_user_rids);

    if (!empty($this_user_rids)) {

    $query = db_select('existing_role_combinations')
    ->fields('existing_role_combinations')
    ->condition('rids', $this_user_rids, '=')
    ->execute()
    ->fetchAssoc();

    if (is_array($query)) {
      $context['message'] = 'rids are already in table, skipping';
    }

    else {
      $context['message'] = 'adding unique rids to table';

      db_insert('existing_role_combinations')
        ->fields(array(
          'rids' => $this_user_rids,
          ))
        ->execute();
    }
  }
}
}



function batch_identify_unique_role_combinations($start, $end, $max) {

  if ($start == $end) {
    for ($i = 1; $i <= $max; $i++) {
      $operations[] = array('indentify_unique_role_combinations', array($i));
      //$operations []= array();
    }
  }
  else {
  for ($i = $start; $i <= $end; $i++) {
      $operations[] = array('indentify_unique_role_combinations', array($i));
      //$operations []= array();
    }
  }
//$start_memory = memory_get_usage();
  $batch = array(
    'title' => t('Identifying unique role combinations'),
    'operations' => $operations,
    'finished' => 'identify_unique_role_combinations_finished',
    'file' => drupal_get_path('module', 'existing_role_combinations') . '/existing_role_combinations.page.inc',
  );
//dpm($batch);

//dpm(memory_get_usage() - $start_memory);
  return $batch;


}

function identify_unique_role_combinations_finished($success, $results, $operations) {
  if ($success){
    drupal_set_message(t('Success!'));
    $time = time();
    variable_set('existing_role_combinations_last_run', $time);
  }
  else {
    drupal_set_message(t('Not success'));
  }
}


