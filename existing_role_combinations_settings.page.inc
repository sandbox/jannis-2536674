<?php


/*
* Custom page callback for webform_submission_cleaner
*/

function existing_role_combinations_settings_page() {

  return drupal_get_form('form_existing_role_combinations_settings');
 }

function form_existing_role_combinations_settings($form_state) {
  $form = array();
  $currentsetting = variable_get('authcache_enum_role_combine');
  $form['role_combiner'] = array(
    '#type' => 'radios',
    '#title' => t('Select a method of providing role combinations to authcache enumeration'),
    '#options' => array(
      '_existing_role_combinations_role_combine_on_the_fly' => t('Calculate existing role combinations on the fly.'),
      '_existing_role_combinations_role_combine' => t('Calculate using maintained database table of existing role combinations'),
    ),
    '#default_value' => $currentsetting,
  );
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
);
  return $form;
}




 //Validate the form state from the user
 function form_existing_role_combinations_settings_validate($form, $form_state) {
  $role_combiner = $form_state['values']['role_combiner'];
  variable_set('authcache_enum_role_combine', $role_combiner);
}
